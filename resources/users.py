from flask_jwt_extended import jwt_required
from flask_restx import Namespace, Resource, fields

from models.money.userModel import User
from utils.common import pagination_parser

user_ns = Namespace('users', description='User operations')

user_model = user_ns.model('User', {
    'id': fields.Integer(required=True, description='User ID'),
    'name': fields.String(required=True, description='User Name'),
    'email': fields.String(required=True, description='User Email')
})


@user_ns.route('', methods=['GET'])
@user_ns.response(404, 'User not found')
class UserListResource(Resource):
    @user_ns.expect(pagination_parser)
    @user_ns.marshal_list_with(user_model)
    @jwt_required()
    def get(self):
        args = pagination_parser.parse_args()
        page = args['page']
        per_page = args['per_page']

        users = User.query.paginate(page=page, per_page=per_page)
        return users.items, 200, {'X-Total-Count': users.total, 'X-Page': page, 'X-Per-Page': per_page}


@user_ns.route('/<int:id>', methods=['GET'])
@user_ns.response(404, 'User not found')
@user_ns.param('id', 'The user identifier')
class UserResource(Resource):
    @user_ns.marshal_with(user_model)
    @jwt_required()
    def get(self, id):
        user = User.query.get_or_404(id, description="User not found")
        return user
