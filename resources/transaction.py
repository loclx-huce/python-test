from flask_jwt_extended import jwt_required
from flask_restx import Namespace, Resource, fields

from models.money.transactionModel import Transaction
from utils.common import pagination_parser

transaction_ns = Namespace('transactions', description='Transaction operations')

transaction_model = transaction_ns.model('Transaction', {
    'id': fields.Integer(required=False, description='Transaction ID'),
    'user_id': fields.Integer(required=True, description='User ID'),
    'amount': fields.Float(required=True, description='Transaction Amount'),
    'description': fields.String(required=True, description='Transaction Description')
})


@transaction_ns.route('/user/<int:user_id>')
@transaction_ns.response(404, 'User not found')
@transaction_ns.param('user_id', 'The user identifier')
class TransactionListResource(Resource):
    @transaction_ns.expect(pagination_parser)
    @transaction_ns.marshal_list_with(transaction_model)
    @jwt_required()
    def get(self, user_id):
        args = pagination_parser.parse_args()
        page = args['page']
        per_page = args['per_page']

        transactions = Transaction.query.filter_by(user_id=user_id).paginate(page=page, per_page=per_page)

        return transactions.items, 200, {'X-Total-Count': transactions.total, 'X-Page': page, 'X-Per-Page': per_page}
