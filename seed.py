import random

from extensions import db, app
from models.money.transactionModel import Transaction
from models.money.userModel import User

with app.app_context():
    db.drop_all()
    db.create_all()

    names = ['Leonardo DiCaprio', 'Taylor Swift', 'Cristiano Ronaldo', 'Oprah Winfrey',
             'Elon Musk', 'Ariana Grande', 'Beyoncé', 'Dwayne Johnson', 'Emma Watson', 'LeBron James']

    users = []
    for name in names:
        user = User(name=name, email=f"{name.replace(' ', '.').lower()}@gmail.leloc.com", password_hash="123456")
        users.append(user)
        db.session.add(user)
    db.session.commit()

    for user in users:
        num_transactions = random.randint(3, 10)
        for _ in range(num_transactions):
            amount = round(random.uniform(100_000.0, 5_000_000.0), 2)
            desc = f"Purchase {random.randint(1, 100)}"
            transaction = Transaction(user_id=user.id, amount=amount, description=desc)
            db.session.add(transaction)
        db.session.commit()