from flask import Flask
from extensions import db, ma, api, jwt
from config import Config
from resources.authen import auth_ns
from resources.transaction import transaction_ns
from resources.users import user_ns


def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)

    db.init_app(app)
    ma.init_app(app)
    api.init_app(app)
    jwt.init_app(app)

    api.add_namespace(user_ns, path='/users')
    api.add_namespace(transaction_ns, path='/transactions')
    api.add_namespace(auth_ns, path='/auth')

    with app.app_context():
        db.create_all()

    return app


if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)
