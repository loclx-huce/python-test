import unittest
from app import create_app
from extensions import db
from models.money.userModel import User


class UserTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app()
        self.app.config['TESTING'] = True
        self.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        self.client = self.app.test_client()

        with self.app.app_context():
            db.create_all()

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()

    def test_get_users(self):
        with self.app.app_context():
            user = User(name="Test User", email="test@leloc.com")
            db.session.add(user)
            db.session.commit()

        response = self.client.get('/users')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json), 1)
        self.assertEqual(response.json[0]['name'], 'Test User')
        self.assertEqual(response.json[0]['email'], 'test@leloc.com')


if __name__ == '__main__':
    unittest.main()
