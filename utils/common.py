from flask_restx import reqparse

pagination_parser = reqparse.RequestParser()
pagination_parser.add_argument('page', type=int, required=False, default=1, help='Page number')
pagination_parser.add_argument('per_page', type=int, required=False, default=10,
                               help='Results per page')
